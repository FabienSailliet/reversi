package saillietliagre.othello;

/**
 * Visible color of a piece
 *
 * @author liagrec
 */
public enum Color {
	WHITE,
	BLACK;
	
	public Color flip() {
		return (this == WHITE) ?  BLACK : WHITE;
	}
	
	public String toString() {
		return Character.toString((char) ((this == WHITE) ? 9675 : 9679)); // Return Unicode char
	}
}
