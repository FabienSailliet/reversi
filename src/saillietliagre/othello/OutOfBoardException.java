package saillietliagre.othello;

/**
 * The selected square is out of the board
 * 
 * @author fabien
 *
 */
public class OutOfBoardException extends InvalidSquareException {
	
	/**
	 * Construct a new OutOfBoardException.
	 * 
	 * @param message detailed message
	 */
	public OutOfBoardException(final String message) {
		super(message);
	}
	
}
