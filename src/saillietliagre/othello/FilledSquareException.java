package saillietliagre.othello;

/**
 * The selected square is already filled
 * 
 * @author fabien
 *
 */
public class FilledSquareException extends InvalidSquareException {
	/**
	 * Constructs a new exception with the specified detail message.  The
	 * cause is not initialized, and may subsequently be initialized by
	 * a call to {@link #initCause}.
	 *
	 * @param message the detail message. The detail message is saved for
	 *                later retrieval by the {@link #getMessage()} method.
	 */
	public FilledSquareException(final String message) {
		super(message);
	}
}
