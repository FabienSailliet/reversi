package saillietliagre.othello;

import java.io.IOException;
import java.util.Scanner;

/**
 * Manage game turn, inputs, victory
 *
 * @author fabien
 */
public class Game {
	/** Board of the Game. */
	private Board board;
	/** First player of the game. */
	private Player player1;
	/** Second player of the game. */
	private Player player2;
	/** Scanner */
	private Scanner sc;

	/**
	 * Creates a new game.
	 */
	public Game(final String[] names, final Scanner sc) {
		this.board = new Board();
		this.player1 = new Player(names[0], Color.WHITE);
		this.player2 = new Player(names[1], Color.BLACK);
		this.sc = sc;
		System.out.println("\n" + player1.getRep() + " " + player1 + "\n" + player2.getRep() + " " + player2);
	}

	/**
	 * Start the game, return when it is finished
	 * 
	 * @throws IOException
	 */
	public void start() {

		Player currentPlayer = player1;
		Player nextPlayer = player2;
		boolean hasNextLoop = true;

		// Main game loop
		do {

			System.out.println("-----------------------------------------------------\n");
			System.out.println(this.board);
			System.out.printf("It's your turn %s!\n%n", currentPlayer);

			// If board is filled
			if (this.board.isFilled()) {
				System.out.println("The board is filled!");
				hasNextLoop = false;
			}
			// If current player can play
			else if (board.canPlay(currentPlayer.getColor())) {
				play(currentPlayer);
			}
			// If other player can play
			else if (board.canPlay(nextPlayer.getColor())) {
				System.out.println("You cannot play, pass your turn!");
				// Wait until player press enter
				sc.nextLine();
			}
			// If nobody can play
			else {
				System.out.println("Nobody can play!");
				hasNextLoop = false;
			}

			Player p = currentPlayer;
			currentPlayer = nextPlayer;
			nextPlayer = p;

		} while (hasNextLoop);
	}
	
	/**
	 * Display the winner, and number of points.
	 */
	public void displayStats() {
		System.out.println("\n------------------------------\n");
		System.out.println("Game is finished !\n");
		System.out.println(this.board + "\n");

		int p1Score = this.board.getPiecesCount(this.player1.getColor());
		int p2Score = this.board.getPiecesCount(this.player2.getColor());

		System.out.println(this.player1 + " has " + p1Score + " pieces.");
		System.out.println(this.player2 + " has " + p2Score + " pieces.\n");

		if (p1Score == p2Score) {
			System.out.println("Equality, nobody wins");
		} else {
			Player winner = p1Score < p2Score ? this.player2 : this.player1;
			System.out.println(winner + " wins.");
		}
	}
	
	/**
	 * Ask a player for a place to put a piece on
	 * 
	 * @param currentPlayer player who plays
	 */
	private void play(Player currentPlayer) {
		System.out.print("Select a square to put a piece on: ");

		// While selection isn't correct
		boolean ok;
		do {
			ok = true;
			String s = sc.nextLine();

			// Check square selection
			try {
				int flippedPieces = board.putPiece(currentPlayer.getColor(), s);

				System.out.println("You have flipped " + flippedPieces + " other pieces.\n");
			} catch (InvalidSquareException e) {
				System.out.print("\n" + e.getMessage() + ": ");
				ok = false;
			}

		} while (!ok);
	}

}
