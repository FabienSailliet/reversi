package saillietliagre.othello;

/**
 * @author liagrec
 */
public class Player {
	/** Color of the player's pieces */
	private final Color color;
	/** Displayed name */
	private final String name;
	
	// Constructors
	
	/**
	 * Creates a new player
	 *
	 * @param b    game board
	 * @param c    player color
	 * @param name displayed name
	 */
	public Player(String name, Color c) {
		this.color = c;
		this.name = name;
	}
	
	/**
	 * Return player chosen name
	 *
	 * @return player name
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Return player's graphical representation
	 * 
	 * @return player's graphical representation on the board
	 */
	public String getRep() {
		return this.color.toString();
	}
	
	/**
	 * Return player's name
	 */
	@Override
	public String toString() {
		return this.name;
	}
	
	public Color getColor() {
		return this.color;
	}
	
}
