package saillietliagre.othello;

/**
 * The selected piece is not in the same line, column or diagonal as another one with the same color
 * 
 * @author fabien
 *
 */
public class NoOtherPieceInlineException extends InvalidSquareException {
	
	/**
	 * Construct a new NoOtherPieceInlineException.
	 * 
	 * @param message detailed message
	 */
	public NoOtherPieceInlineException(String message) {
		super(message);
	}
	
}
