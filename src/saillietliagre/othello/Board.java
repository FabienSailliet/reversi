package saillietliagre.othello;

/**
 * Game board representation, containing squares.
 *
 * @author fabien
 */
public class Board {
	/** Number of columns on the board */
	public static final int WIDTH  = 8;
	/** Number of rows on the board */
	public static final int HEIGHT = 8;
	
	/** Squares of the board */
	private Color[][] pieces;
	
	// Constructors
	
	/**
	 * Create a new empty board
	 */
	public Board() {
		// Creates and initializes pieces
		pieces = new Color[Board.WIDTH][Board.HEIGHT];
		
		// Place default pieces
		pieces[3][3] = Color.WHITE;
		pieces[4][4] = Color.WHITE;
		pieces[3][4] = Color.BLACK;
		pieces[4][3] = Color.BLACK;
	}
	
	// Getters
	
	/**
	 * Count pieces on the board for a player
	 * 
	 * @param color color of the player
	 * @return number of player's pieces placed on the board
	 */
	public int getPiecesCount(Color color) {
		int c = 0;
		
		for (Color[] row: this.pieces) {
			for (Color piece: row) {
				if (piece == color) {
					c++;
				}
			}
		}
		
		return c;
	}
	
	// Setters
	
	// Others
	
	/**
	 * Are all the places of the board filled
	 *
	 * @return true if there is no empty place
	 */
	public boolean isFilled() {
		// Search empty square
		for (Color[] row : pieces) {
			for (Color p : row) {
				// If empty place has been found
				if (p == null) {
					return false;
				}
			}
		}
		
		// If no empty place
		return true;
	}
	
	/**
	 * Check if a player can place a piece on the board
	 *
	 * @param c color of the player pieces
	 *
	 * @return true if the player can place a piece
	 */
	public boolean canPlay(Color c) {
		
		for (int row = 0 ; row < Board.HEIGHT ; row++) {
			for (int col = 0 ; col < Board.WIDTH ; col++) {
				try {
					this.testPlacement(c, row, col);
					
					// If can place in this case
					return true;
				}
				catch (InvalidSquareException e) {}
			}
		}
		
		return false;
	}
	
	/**
	 * Check if a player can place a piece at a chosen place
	 * 
	 * @param c color of the player
	 * @param x
	 * @param y
	 * @throws InvalidSquareException if the piece cannot be place at the chosen place
	 */
	private void testPlacement(Color c, int x, int y) throws InvalidSquareException {
		Color piece;
		
		// Test if selected square is on the board
		try {
			piece = pieces[x][y];
		}
		catch (ArrayIndexOutOfBoundsException e) {
			throw new OutOfBoardException("Selected square is out of the board, please choose another one");
		}
		
		// If selected place is filled
		if (piece != null) {
			throw new FilledSquareException("Selected square is already filled, please choose another one");
		}
		else {
			boolean validNeighbour = false;
			
			// Iterating threw nearby places
			for (int xd : new int[]{-1, 0, 1}) {
				for (int yd : new int[]{-1, 0, 1}) {
					if (xd != 0 || yd != 0) {
						
						Color nearP;
						
						// Test if nearby place is on the board
						try {
							nearP = pieces[x + xd][y + yd];
						}
						catch (IndexOutOfBoundsException e) {
							continue;
						}
						
						// If near piece exists and has a different color
						if (nearP != null && nearP != c) {
							validNeighbour = true;
							
							try {
								Color alignedPiece;
								
								// Search for a piece of the same color as the player on the line
								int i = 2;
								do {
									alignedPiece = pieces[x + i * xd][y + i * yd];
									i++;
								} while (alignedPiece != c && alignedPiece != null);
								
								// We can place the piece
								if (alignedPiece == c) {
									return;
								}
							}
							catch (IndexOutOfBoundsException | NullPointerException e) {}
						}
						
					}
				}
			}
			
			// If there is no nearby piece of a different color
			if (!validNeighbour) {
				throw new NoValidNeighbourException("Selected square has no nearby piece of another color, please choose another one");
			}
			// If there is no piece of the same color aligned with this one
			else {
				throw new NoOtherPieceInlineException("You don't have another piece in the same column, line or diagonal of the selected square. Please choose another one");
			}
		}
		
		
	}
	
	
	/**
	 * Put a new piece on the board
	 *
	 * @param c color of the new piece
	 * @param s square selection
	 * @throws InvalidSquareException 
	 */
	public int putPiece(Color c, String s) throws InvalidSquareException {
		
		int x, y;
		
		try {
			x = Character.toLowerCase(s.charAt(0)) - 'a';
			y = s.charAt(1) - '0' - 1;
		}
		catch (IndexOutOfBoundsException e) {
			throw new InvalidSquareSelectionException("Invalid selection, please retype (ex: a1)");
		}
		
		this.testPlacement(c, x, y);
		
		// We can place the piece here
		
		// Flipped pieces of the other color
		int flippedPieces = 0;
		
		// Iterating threw nearby places
		for (int xd : new int[]{-1, 0, 1}) {
			for (int yd : new int[]{-1, 0, 1}) {
				if (xd != 0 || yd != 0) {
					
					Color nearP;
					
					// Test if nearby place is on the board
					try {
						nearP = pieces[x + xd][y + yd];
					}
					catch (IndexOutOfBoundsException e) {
						continue;
					}
					
					// If near piece exists and has a different color
					if (nearP != null && nearP != c) {
						
						try {
							Color alignedPiece;
							
							// Search for a piece of the same color as the player on the line
							int i = 2;
							do {
								alignedPiece = pieces[x + i * xd][y + i * yd];
								i++;
							} while (alignedPiece != c && alignedPiece != null);
							
							// We can place and flip pieces
							if (alignedPiece == c) {
								
								flippedPieces += i-2;
								
								// Place the new piece
								pieces[x][y] = c;
								
								for (i-=2 ; i>0 ; i--) {
									pieces[x + i * xd][y + i * yd] = pieces[x + i * xd][y + i * yd].flip();
								}
								
							}
						}
						catch (IndexOutOfBoundsException | NullPointerException e) {}
					}
					
				}
			}
		}
		
		return flippedPieces;
	}
	
	/**
	 * Return Unicode-styled board
	 */
	public String toString() {
		final char HORIZONTAL_LINE_CHAR = (char) 9472;
		final char VERTICAL_LINE_CHAR = (char) 9474;
		final char CROSS_CHAR = (char) 9532;
		
		String str = "", colsLine = "", rowSepLine = "";
		
		// Set first and last line with columns letters
		colsLine += " ";
		for (int i = 0; i < Board.WIDTH; i++) {
			colsLine += "   " + (char) ('A' + i);
		}
		
		// Set row separation line
		rowSepLine += "  ";
		for (int i = 0; i < Board.WIDTH; i++) {
			rowSepLine += CROSS_CHAR + repeat(HORIZONTAL_LINE_CHAR, 3);
		}
		rowSepLine += CROSS_CHAR + "\n";
		
		// Insert columns letters line
		str += colsLine + "\n";
		
		// Insert row separation line
		str += rowSepLine;
		
		// Iterating threw squares array
		for (int row = 0; row < Board.HEIGHT; row++) {
			
			// Insert line number
			str += row + 1;
			
			for (int col = 0; col < Board.WIDTH; col++) {
				Color c = this.pieces[col][row];
				
				str += " " + VERTICAL_LINE_CHAR + " ";
				
				// If there is no piece in the square
				if (c == null) {
					str += ' ';
				}
				else {
					str += c;
				}
				
			}
			str += " " + VERTICAL_LINE_CHAR + " ";
			
			// Insert line number
			str += row + 1;
			
			str += "\n";
			
			// Insert row separation line
			str += rowSepLine;
		}
		
		// Insert columns letters line
		str += colsLine + "\n";
		
		return str;
	}
	
	/**
	 * Repeat a String
	 * @param s String to repeat
	 * @param n times to repeat
	 * @return repeated String
	 */
	private String repeat(String s, int n) {
		String ret = "";
		
		for (int i=0 ; i<n ; i++) {
			ret += s;
		}
		
		return ret;
	}
	
	/**
	 * Repeat a char
	 * @param c char to repeat
	 * @param n times to repeat
	 * @return repeated char in String
	 */
	private String repeat(char c, int n) {
		return repeat(Character.toString(c), n);
	}
}
