package saillietliagre.othello;

/**
 * The player cannot place a piece at the selected square
 * 
 * @author fabien
 *
 */
public class InvalidSquareException extends Exception {
	
	/**
	 * Construct a new InvalidSquareException
	 * 
	 * @param message detailed message
	 */
	public InvalidSquareException(final String message) {
		super(message);
	}
}
