package saillietliagre.othello;

/**
 * The square selection isn't valid (ex: empty selection string, a instead of a1)
 * 
 * @author fabien
 *
 */
public class InvalidSquareSelectionException extends InvalidSquareException {
	
	/**
	 * Construct a new InvalidSquareSelectionException.
	 * 
	 * @param message detailed message
	 */
	public InvalidSquareSelectionException(String message) {
		super(message);
	}
	
}
