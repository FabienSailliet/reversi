package saillietliagre.othello;

/**
 * The selected piece has no nearby piece of the opposite color
 * 
 * @author fabien
 *
 */
public class NoValidNeighbourException extends InvalidSquareException {
	/**
	 * Constructs a new exception with the specified detail message.  The
	 * cause is not initialized, and may subsequently be initialized by
	 * a call to {@link #initCause}.
	 *
	 * @param message the detail message. The detail message is saved for
	 *                later retrieval by the {@link #getMessage()} method.
	 */
	public NoValidNeighbourException(final String message) {
		super(message);
	}
}
