package saillietliagre.othello;

import java.util.Scanner;

/**
 * Start the game and manage restart
 *
 * @author fabien
 */
public class Othello {
	/** Default players' name. */
	private static final String[] DEF_PLAYERS_NAME = { "player1", "player2" };
	
	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in)) {
			
			String[] names = new String[2];
			
			System.out.println("Welcome in the Othello game !\n");
			for (int i = 0; i < 2; i++) {
				System.out.printf("Enter a name for player%d (def: %s) : ", i, DEF_PLAYERS_NAME[i]);
				names[i] = sc.nextLine();
				
				// Use default name if necessary
				if ("".equals(names[i])) {
					names[i] = DEF_PLAYERS_NAME[i];
				}
			}
			Game game = new Game(names, sc);
			game.start();
			game.displayStats();
		}
	}
}
